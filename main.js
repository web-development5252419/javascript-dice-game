'use strict';

let currentScore = 0;
let activePlayer = 1;
let playerScores = [0, 0];
let playing = true;

const playerOneUI = document.getElementById('player1');
const playerTwoUI = document.getElementById('player2');
const rollDiceButton = document.querySelector('.roll-dice');
const holdScoreButton = document.querySelector('.hold-score');
const newGameButton = document.querySelector('.new-game');
const diceContainerElement = document.querySelector('.dice-container');
const diceElement = document.querySelectorAll('.dice');

const setElementIdTextContent = function (id, value) {
    document.getElementById(`${id}`).textContent = value;
}

const playDiceRollAnimation = function (diceElement) {
    diceElement.classList.remove('dice-animation');
    void diceElement.offsetWidth;
    diceElement.classList.add('dice-animation');
}

const switchActivePlayer = function () {
    setElementIdTextContent(`current-score${activePlayer}`, 0);
    activePlayer = activePlayer === 1 ? 2 : 1;
    currentScore = 0;
    playerOneUI.classList.toggle('player-inactive');
    playerTwoUI.classList.toggle('player-inactive');
}

rollDiceButton.addEventListener('click', function () {
    if (playing) {
        diceContainerElement.classList.remove('hidden');
        const diceRollValue = Math.trunc(Math.random() * 6) + 1;

        diceElement.forEach(element => {
            playDiceRollAnimation(element);

            if (!element.classList.contains('hidden')) {
                element.classList.add('hidden');
            }
            if (element.className.includes(diceRollValue.toString())) {
                element.classList.remove('hidden');
            }
        });

        if (diceRollValue !== 1) {
            currentScore += diceRollValue;
            setElementIdTextContent(`current-score${activePlayer}`, currentScore);
        } else {
            switchActivePlayer();
        }
    }
});

holdScoreButton.addEventListener('click', function () {
    if (playing) {
        playerScores[activePlayer - 1] += currentScore;
        setElementIdTextContent(`total-score${activePlayer}`, playerScores[activePlayer - 1]);

        if (playerScores[activePlayer -1] >= 100) {
            playing = false;
            document.getElementById(`player${activePlayer}`).classList.add('player-winner');
            rollDiceButton.style.opacity = '.5';
            holdScoreButton.style.opacity = '.5';
        } else {
            switchActivePlayer();
        }
    }
});

newGameButton.addEventListener('click', function () {
    setElementIdTextContent('total-score1', 0);
    setElementIdTextContent('total-score2', 0);
    setElementIdTextContent('current-score1', 0);
    setElementIdTextContent('current-score2', 0);

    document.getElementById(`player${activePlayer}`).classList.remove('player-winner');
    playerOneUI.classList.remove('player-inactive');
    playerTwoUI.classList.add('player-inactive');
    diceContainerElement.classList.add('hidden');

    rollDiceButton.style.opacity = '1';
    holdScoreButton.style.opacity = '1';

    playing = true;
    currentScore = 0;
    playerScores = [0, 0];
    activePlayer = 1;
});