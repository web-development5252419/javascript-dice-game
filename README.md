<div align="center">
<h1>
Javascript Pig Game<br/>
https://javascript-dice-game-web-development5252419-05ff40e2881d188adf9.gitlab.io/ <br/>
</h1>
</div>
This is a JavaScript game where I used concepts like manipulating CSS classes, click event handling, DOM manipulation, the use of ternary operators, iteration, template literals, and reusable functions to maintain code reusability, maintainabilty and readability while adhering to the DRY principle.<br/>

The game rules are as follows:<br/>

-- the first player to reach 100 total score points wins<br/>

-- player 1 is the first to roll the dice<br/>

-- each dice roll value is added to the current score of the active player but if the value is 1, the current player loses all the current score points and cannot roll again until the next player rolls a 1 or holds the current score<br/>

-- to avoid rolling a 1, you should roll the dice only a few times, then, press the hold button to hold the current score<br/>

-- the current score is added to the total score after the player presses the hold button
